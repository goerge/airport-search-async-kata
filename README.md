# Async Airport Search Kata

Original kata by [Wolfram Kriesing](https://twitter.com/wolframkriesing/status/870335115184021506), used and described in [jslang-meetup-async-await](http://techblog.holidaycheck.com/post/2017/06/02/jslang-meetup-async-await). The kata explores `Promise` and `async/await` of ES6.

## Requirements

See picture of required workflow:
![Search for nearest airport](AsyncAirportSearchKata.jpg)

## License

[New BSD License](http://opensource.org/licenses/bsd-license.php), see `license.txt` in repository.
