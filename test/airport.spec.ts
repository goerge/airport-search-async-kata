import {expect} from "chai";

interface Coordinate {
    latitude: number;
    longitude: number;
}

class GPS {
    public async getCoordinate(): Promise<Coordinate> {
        return Promise.reject();
    }
}

class ListOfAirports {
    public async allAirports(): Promise<string[]> {
        return Promise.reject();
    }

    public async nearestAirport(coordinate: Coordinate): Promise<string> {
        return Promise.reject();
    }
}

class AirportSearch {
    private gps: GPS;
    private listOfAirports: ListOfAirports;

    constructor(gps: GPS, listOfAirports: ListOfAirports) {
        this.gps = gps;
        this.listOfAirports = listOfAirports;
    }

    public async searchAirportsAsync() {
        let coordinate;
        try {
            coordinate = await this.gps.getCoordinate();
        } catch (e) {
            return await this.listOfAirports.allAirports();
        }
        try {
            return await this.listOfAirports.nearestAirport(coordinate);
        } catch (e) {
            throw new Error(`could not find nearest airport: ${e.message}`);
        }
    }

    public async searchAirportsDoesNotWork() {
        return this.gps.getCoordinate()
            .then((coordinate) => this.listOfAirports.nearestAirport(coordinate)
                .catch((e) => {
                    throw new Error(`could not find nearest airport: ${e.message}`);
                    // this error is caught in outside catch. would it be a status message all would be well.
                }))
            .catch((e) => this.listOfAirports.allAirports());
    }

    public async searchAirportsPromiseV1() {
        return this.gps.getCoordinate()
            .then((coordinate) => this.listOfAirports.nearestAirport(coordinate))
            .catch((e) => {
                const m = e.message;
                if (m === "inside concrete") {
                    return this.listOfAirports.allAirports();
                } else {
                    throw new Error(`could not find nearest airport: ${e.message}`);
                }
            });
    }

    public async searchAirportsV2() {
        return this.gps.getCoordinate()
            .catch((e) => this.listOfAirports.allAirports())
            .then((sth) => {
                if (sth instanceof Array) {
                    return sth as any; // why compiler not understand types?
                } else {
                    return this.listOfAirports.nearestAirport(sth)
                        .catch((e) => {
                            throw new Error(`could not find nearest airport: ${e.message}`);
                        });
                }
            });
    }

    public async searchAirports() {
        let coordinate;
        try {
            coordinate = await this.gps.getCoordinate();
        } catch (e) {
            return await this.listOfAirports.allAirports();
        }
        try {
            return await this.listOfAirports.nearestAirport(coordinate);
        } catch (e) {
            throw new Error(`could not find nearest airport: ${e.message}`);
        }
    }
}

describe("Airport Search", () => {

    let gps: GPS;
    let listOfAirports: ListOfAirports;
    let search: AirportSearch;

    beforeEach(() => {
        gps = new GPS();
        listOfAirports = new ListOfAirports();
        search = new AirportSearch(gps, listOfAirports);
    });

    function givenGpsCoordinateAs(coordinate: Coordinate) {
        gps.getCoordinate = async () => coordinate;
    }

    function givenGpsFails() {
        gps.getCoordinate = async () => {
            throw new Error("inside concrete");
        };
    }

    function givenNearestAirportFor(airportCoordinate: Coordinate) {
        return {
            toBe(airportName: string) {
                listOfAirports.nearestAirport = async (coordinate: Coordinate) => airportName;
            },
        };
    }

    function givenNearestAirportSearchFails() {
        listOfAirports.nearestAirport = async (coordinate: Coordinate) => {
            throw new Error("service not available");
        };
    }

    function givenAllAirportsAre(airports: string[]) {
        listOfAirports.allAirports = async () => airports;
    }

    it("receives GPS coordinates and finds nearest airport", async () => {
        givenGpsCoordinateAs({latitude: 10, longitude: 20});
        givenNearestAirportFor({latitude: 10, longitude: 20}).toBe("VIE");

        const airportName = await search.searchAirports();

        expect(airportName).to.equal("VIE");
    });

    it("fails when list of airports is not available", async () => {
        givenGpsCoordinateAs({latitude: 10, longitude: 20});
        givenNearestAirportSearchFails();

        try {
            await search.searchAirports();
        } catch (e) {
            expect(e.message).to.equals("could not find nearest airport: service not available");
        }
    });

    it("lists all airports when GPS fails", async () => {
        givenGpsFails();
        givenAllAirportsAre(["VIE", "HAM", "BER"]);

        const allAirportNames = await search.searchAirports();

        expect(allAirportNames).to.deep.equals(["VIE", "HAM", "BER"]);
    });

    // list of airport timeout
});
